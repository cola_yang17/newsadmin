// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import axios from 'axios'

// 引入css
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false

// 注册组件库
Vue.use(ElementUI);
// 将axios绑定到原型上
Vue.prototype.$axios = axios;
// 设置默认的域名
axios.defaults.baseURL = "http://111.230.181.206:3000"

router.beforeEach((to, from, next) => {
  if (to.path != '/login') {
    const token = localStorage.getItem('token');
    if (!token) {
      router.push('/login');
    } else {
      next()
    }
  } else {
    next()
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
