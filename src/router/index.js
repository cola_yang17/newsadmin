import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Login from '@/pages/Login'
import EditPost from '@/pages/EditPost'
import PostList from '@/pages/PostList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homePage',
      component: Home,
      children:[
        {
          path:'editpost',
          name:'editPostPage',
          component:EditPost
        },
        {
          path:'postlist',
          name:'postListPage',
          component:PostList
        }
      ]
    },
    {
      path:'/login',
      name:'loginPage',
      component:Login
    },
  ]
})
